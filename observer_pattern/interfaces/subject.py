from abc import ABC, abstractmethod
from interfaces.observer import Observer

class Subject(ABC): 
    """ 
        Take as argument, Observer Interface 
    """
    @abstractmethod
    def registerObserver(self, observer: Observer):
        pass

    """ 
        Take as argument, Observer Interface 
    """
    @abstractmethod
    def removeObserver(self, observer: Observer):
        pass

    @abstractmethod
    def notifyObserver(self):
        pass
