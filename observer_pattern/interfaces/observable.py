from abc import ABC, abstractmethod


class Observable(ABC):
    @abstractmethod
    def addObserver(self):
        pass

    @abstractmethod
    def deleteObserver(self):
        pass

    @abstractmethod
    def notifyObservers(self):
        pass
    
    @abstractmethod
    def setChanged(self):
        pass