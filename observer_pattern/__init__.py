from subjects.weather_data import WeatherData 
from displays.current_conditions_display import CurrentConditionsDisplay
from displays.forecast_display import ForecastDisplay
from displays.statics_display import StaticsDisplay

subject = WeatherData()
observer_1 = CurrentConditionsDisplay(subject)
observer_2 = StaticsDisplay()
observer_3 = ForecastDisplay()
subject.registerObserver(observer_2)
subject.registerObserver(observer_3)
print("---- Obtained Data For Weather Station ----")
subject.setMeasurements(80, 65, 30.4)
print("---- Obtained Data For Weather Station ----")
subject.setMeasurements(82, 70, 29.2)
print("---- Obtained Data For Weather Station ----")
subject.setMeasurements(78, 90, 29.2)