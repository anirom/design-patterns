from interfaces.observer import Observer
from interfaces.display_element import DisplayElement
from subjects.weather_data import WeatherData


class StaticsDisplay(Observer, DisplayElement):
    def __init__(self):
        self.max_temp = 0
        self.min_temp = 200
        self.temp_sum = 0
        self.num_readings = 0

    def update(self, weatherData: WeatherData):
        temp = weatherData.getTemperature()
        self.temp_sum += temp
        self.num_readings += 1

        if temp > self.max_temp:
            self.max_temp = temp
        
        if temp < self.min_temp:
            self.min_temp = temp

        self.display()

           
    def display(self):
        print(f"Avg/Max/Min temperature = {self.temp_sum / self.num_readings} / {self.max_temp} / {self.min_temp}")