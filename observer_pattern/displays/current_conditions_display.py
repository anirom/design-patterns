from interfaces.observer import Observer
from interfaces.observable import Observable
from interfaces.display_element import DisplayElement
from subjects.weather_data import WeatherData


class CurrentConditionsDisplay(Observer, DisplayElement):
    def __init__(self, observable: Observable):
        self.observable = observable
        self.observable.addObserver(self)
        
        self.temperature = None
        self.humidity = None

    def update(self, observer: Observable):
            self.temperature = observer.temperature
            self.humidity = observer.humidity
            self.display()

           
    def display(self):
        print(f"Current conditions: {self.temperature}F degrees and {self.humidity}% humidity")

