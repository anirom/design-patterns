from interfaces.observer import Observer
from interfaces.display_element import DisplayElement
from subjects.weather_data import WeatherData


class ForecastDisplay(Observer, DisplayElement):
    def __init__(self):
        self.current_pressure = 29.9
        self.last_pressure = 0

    def update(self, weatherData: WeatherData): 
        self.last_pressure =  self.current_pressure
        self.current_pressure = weatherData.getPressure()
        
        self.display()

           
    def display(self):
        if self.current_pressure > self.last_pressure:
            print("Forecast: Improving weather on the way!")
        elif self.current_pressure == self.last_pressure:
            print("Forecast: More of the same")
        elif self.current_pressure < self.last_pressure:
            print("Forecast: Watch out for cooler, rainy weather")