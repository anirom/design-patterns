from interfaces.observable import Observable
from interfaces.observer import Observer


class WeatherData(Observable):
    def __init__(self):
        self.temperature: float = 0
        self.humidit: float = 0
        self.pressure: float = 0

        self.observers: list = []
        self.changed: bool = False

    def getTemperature(self):
        return self.temperature

    def getHumidity(self):
        return self.humidity

    def getPressure(self):
        return self.pressure

    def registerObserver(self, observer: Observer):
        self.observers.append(observer)
        
    def removeObserver(self, observer: Observer):
        self.observers.remove(observer)

    def addObserver(self, observer):
        if observer not in self.observers:
            self.observers.append(observer)

    def deleteObserver(self, observer):
        self.observers.remove(observer)

    def notifyObservers(self):
        if (self.changed):
            for observer in self.observers:
                observer.update(self)

    def setChanged(self):
        self.changed = True

    def measurementsChanged(self):
        self.setChanged()
        self.notifyObservers()

    def setMeasurements(self, temperature, humidity, pressure):
        self.temperature = temperature
        self.humidity = humidity
        self.pressure = pressure
        
        self.measurementsChanged()
