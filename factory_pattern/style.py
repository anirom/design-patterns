from enum import Enum

class Style(Enum):
    NY = "New York"
    CHICAGO = "Chicago"
    MEXICAN = "Mexican"

class Dough(Enum):
    NY = "Thin Crust Dough"
    CHICAGO = "Extra Thick Crust Dough"
    MEXICAN = "Normal Dough"

class Sauce(Enum):
    NY = "Marinara Sauce"
    CHICAGO = "Plum Tomato Sauce"
    MEXICAN = "Beans Sauce"
