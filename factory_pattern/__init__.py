from stores.ny_style import NYStylePizzaStore
from stores.chicago_style import ChicagoStylePizzaStore
from stores.mexican_style import MexicanStylePizzaStore

ny_pizza = NYStylePizzaStore()
pizza = ny_pizza.orderPizza("house")
print(f"=> Joel ordered {pizza.getName()} \n")

ca_pizza = ChicagoStylePizzaStore()
pizza = ca_pizza.orderPizza("house")
print(f"=> Ethan ordered {pizza.getName()} \n")

ch_pizza = MexicanStylePizzaStore()
pizza = ch_pizza.orderPizza("house")
print(f"=> Lizz ordered {pizza.getName()} \n")