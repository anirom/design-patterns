from pizzas.base import Pizza
from style import Style, Dough, Sauce


class ChicagoHousePizza(Pizza):
    def __init__(self, style:str):
        self.style = style
        self.toppings = []
        
        self.name = f"{style} Style House Pizza"
        self.dough = Dough[Style(style).name].value
        self.sauce = Sauce[Style(style).name].value
        self.toppings.append("Ground beef")
        self.toppings.append("Tomato Sauce")
        self.toppings.append("Pepperoni") 
        self.toppings.append("Onion")
        self.toppings.append("Mushrooms")
        self.toppings.append("Green peppers")
        self.toppings.append("Sprinkle of parmesan cheese")

    def cut(self):
        return print("Cutting the pizza into square slices")

