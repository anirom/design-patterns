from pizzas.base import Pizza
from style import Style, Dough, Sauce


class ChicagoVeggiePizza(Pizza):
    def __init__(self, style:str):
        self.style = style
        self.toppings = []
        
        self.name = f"{style} Style Veggie Pizza"
        self.dough = Dough[Style(style).name].value
        self.sauce = Sauce[Style(style).name].value
        self.toppings.append("Tomato Sauce")
        self.toppings.append("Onion")
        self.toppings.append("Mushrooms")
        self.toppings.append("Green peppers")
        self.toppings.append("Mozarella cheese")

    def cut(self):
        return print("Cutting the pizza into square slices")