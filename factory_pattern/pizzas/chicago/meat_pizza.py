from pizzas.base import Pizza
from style import Style, Dough, Sauce


class ChicagoMeatPizza(Pizza):
    def __init__(self, style:str):
        self.style = style
        self.toppings = []
        
        self.name = f"{style} Style Meat Pizza"
        self.dough = Dough[Style(style).name].value
        self.sauce = Sauce[Style(style).name].value
        self.toppings.append("Ground beef")
        self.toppings.append("Tomato Sauce")
        self.toppings.append("Pepperoni") 
        self.toppings.append("Mozzarella cheese")

    def cut(self):
        return print("Cutting the pizza into square slices")