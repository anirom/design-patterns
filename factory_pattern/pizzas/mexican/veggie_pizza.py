from pizzas.base import Pizza
from style import Style, Dough, Sauce


class MexicanVeggiePizza(Pizza):
    def __init__(self, style:str):
        self.style = style
        self.toppings = []
        
        self.name = f"{style} Style Veggie Pizza"
        self.dough = Dough[Style(style).name].value
        self.sauce = Sauce[Style(style).name].value
        self.toppings.append("Tomato") 
        self.toppings.append("Mushrooms")
        self.toppings.append("Avocado")
        self.toppings.append("Mozzarella cheese")
        