from pizzas.mexican.house_pizza import MexicanHousePizza
from pizzas.mexican.veggie_pizza import MexicanVeggiePizza

from pizzas.chicago.house_pizza import ChicagoHousePizza
from pizzas.chicago.meat_pizza import ChicagoMeatPizza
from pizzas.chicago.veggie_pizza import ChicagoVeggiePizza

from pizzas.new_york.house_pizza import NYHousePizza
from pizzas.new_york.pepperoni_pizza import NYPepperoniPizza
from pizzas.new_york.veggie_pizza import NYVeggiePizza

def __init__():
    return ["MexicanHousePizza", 
            "MexicanVeggiePizza", 
            "ChicagoHousePizza", 
            "ChicagoMeatPizza", 
            "ChicagoVeggiePizza",
            "NYHousePizza",
            "NYPepperoniPizza",
            "NYVeggiePizza"]