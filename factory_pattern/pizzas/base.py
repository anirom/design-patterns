class Pizza:
    def prepare(self):
        print(f"Preparing {self.name}")
        print(f"Tossing {self.dough}")
        print(f"Adding {self.sauce}")
        for topping in self.toppings:
            print(f"Adding {topping}")

    def bake(self):
        print("Baking for 25 minutes at 350")

    def cut(self):
        print("Cutting the pizza into diagonal slice")

    def box(self):
        print("Place pizza in official PizzaStore box")

    def getName(self):
        return self.name