from pizzas.base import Pizza
from style import Style, Dough, Sauce


class NYVeggiePizza(Pizza):
    def __init__(self, style:str):
        self.style = style
        self.toppings = []
        
        self.name = f"{style} Style Veggie Pizza"
        self.dough = Dough[Style(style).name].value
        self.sauce = Sauce[Style(style).name].value
        self.toppings.append("Olives")
        self.toppings.append("Onion")
        self.toppings.append("Mushrooms")
        self.toppings.append("Green pepper") 
        self.toppings.append("Mozarella cheese")