from pizzas.base import Pizza
from style import Style, Dough, Sauce


class NYHousePizza(Pizza):
    def __init__(self, style:str):
        self.style = style
        self.toppings = []
        
        self.name = f"{style} Style House Pizza"
        self.dough = Dough[Style(style).name].value
        self.sauce = Sauce[Style(style).name].value
        self.toppings.append("Mozarella cheese")