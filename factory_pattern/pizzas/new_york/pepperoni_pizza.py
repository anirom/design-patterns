from pizzas.base import Pizza
from style import Style, Dough, Sauce


class NYPepperoniPizza(Pizza):
    def __init__(self, style:str):
        self.style = style
        self.toppings = []
        
        self.name = f"{style} Style Pepperoni Pizza"
        self.dough = Dough[Style(style).name].value
        self.sauce = Sauce[Style(style).name].value
        self.toppings.append("Pepperoni")
        self.toppings.append("Mozarella cheese")