from pizzas import ChicagoHousePizza, ChicagoMeatPizza, ChicagoVeggiePizza
from stores.base import PizzaStore
from style import Style


class ChicagoStylePizzaStore(PizzaStore):
   def __init__(self):
        self.style = Style.CHICAGO.value

   def createPizza(self, pizzaType: str):
      if pizzaType == "house":
         return ChicagoHousePizza(self.style)

      if pizzaType == "meat":
         return ChicagoMeatPizza(self.style)
      
      if pizzaType == "veggie":
         return ChicagoVeggiePizza(self.style)