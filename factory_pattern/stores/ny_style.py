from pizzas import NYHousePizza, NYPepperoniPizza, NYVeggiePizza
from stores.base import PizzaStore
from style import Style


class NYStylePizzaStore(PizzaStore):
    def __init__(self):
        self.style = Style.NY.value

    def createPizza(self, pizzaType: str):
        if pizzaType == "house":
            return NYHousePizza(self.style)

        if pizzaType == "pepperoni":
            return NYPepperoniPizza(self.style)
        
        if pizzaType == "veggie":
            return NYVeggiePizza(self.style)
