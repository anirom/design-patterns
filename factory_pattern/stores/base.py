from abc import abstractmethod, ABCMeta

class PizzaStore(metaclass=ABCMeta):
    def __init__(self):
        self.pizza_store = self
        
    @abstractmethod
    def createPizza(self, pizzaType:str):
        pass
    
    def orderPizza(self, pizzaType:str):
        pizza = self.createPizza(pizzaType)
        
        pizza.prepare()
        pizza.bake()
        pizza.cut()
        pizza.box()

        return pizza
        