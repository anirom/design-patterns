from pizzas import MexicanHousePizza, MexicanVeggiePizza
from stores.base import PizzaStore
from style import Style


class MexicanStylePizzaStore(PizzaStore):
    def __init__(self):
        self.style = Style.MEXICAN.value

    def createPizza(self, pizzaType: str):
        if pizzaType == "house":
            return MexicanHousePizza(self.style)

        if pizzaType == "veggie":
            return MexicanVeggiePizza(self.style)