from ducks.mallard_duck import MallardDuck
from ducks.rubber_duck import RubberDuck
from ducks.model_duck import ModelDuck

from fly_behaviors.fly_rocket_powered import FlyRocketPowered

mallard = MallardDuck()
rubber = RubberDuck()

for bird in (mallard, rubber):
    bird.display()
    bird.quack()
    bird.fly()

model = ModelDuck()
model.display()
model.fly()
model.quack()
model.setFlyBehavior(FlyRocketPowered)
model.fly()
