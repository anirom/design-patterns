from duck import Duck

from fly_behaviors.default import FlyDefault
from quack_behaviors.squeak import Squeak

class RubberDuck(Duck):
    def __init__(self):
        self.flyBehavior = FlyDefault()
        self.quackBehavior = Squeak()

    def display(self):
        print("Im a Rubber Duck")
    