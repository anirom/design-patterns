from duck import Duck

from fly_behaviors.fly_with_wings import FlyWithWings
from quack_behaviors.quack import Quack

class MallardDuck(Duck):
    def __init__(self):
        self.flyBehavior = FlyWithWings("200mts")
        self.quackBehavior = Quack()

    def display(self):
        print("Im a Mallard Duck")
    
