from duck import Duck

from fly_behaviors.default import FlyDefault
from quack_behaviors.default import QuackDefault

class ModelDuck(Duck):
    def __init__(self):
        self.flyBehavior = FlyDefault()
        self.quackBehavior = QuackDefault()

    def display(self):
        print("Im a model duck")