from interfaces.fly_behavior import FlyBehavior
from interfaces.quack_behavior import QuackBehavior

class Duck:
    def __init__(self):
        self.flyBehavior = FlyBehavior()
        self.quackBehavior = QuackBehavior()

    def swim(self):
        print("Default swim")
    
    def display(self):
        print("Default display")

    def fly(self):
        return self.flyBehavior.fly()
        
    def quack(self):
        return self.quackBehavior.quack()

    def setFlyBehavior(self, newFlyBehavior):
        self.flyBehavior = newFlyBehavior()

    def setQuackBehavior(self, newQuackBehavior):
        self.quackBehavior = newQuackBehavior()
       