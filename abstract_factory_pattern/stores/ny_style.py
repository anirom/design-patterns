from stores.base import PizzaStore

from pizzas import HousePizza, PepperoniPizza, VeggiePizza
from ingredients.ny_ingredients import NYIngredientFactory


class NYStylePizzaStore(PizzaStore):
    def createPizza(self, pizzaType:str):
        pizza = None
        ingredients = NYIngredientFactory()

        if pizzaType == "house":
            pizza = HousePizza(ingredients)
            pizza.setName("New York Style House Pizza")

        if pizzaType == "pepperoni":
            pizza = PepperoniPizza(ingredients)
            pizza.setName("New York Style Pepperoni Pizza")
        
        if pizzaType == "veggie":
            pizza = VeggiePizza(ingredients)
            pizza.setName("New York Style Veggie Pizza")

        return pizza
