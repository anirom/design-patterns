from stores.base import PizzaStore

from pizzas import HousePizza, VeggiePizza
from ingredients.mexican_ingredients import MexicanIngredientFactory


class MexicanStylePizzaStore(PizzaStore):
    def createPizza(self, pizzaType:str):
        pizza = None
        ingredients = MexicanIngredientFactory()

        if pizzaType == "house":
            pizza = HousePizza(ingredients)
            pizza.setName("Mexican Style House Pizza")
        
        if pizzaType == "veggie":
            pizza = VeggiePizza(ingredients)
            pizza.setName("Mexican Style Veggie Pizza")

        return pizza