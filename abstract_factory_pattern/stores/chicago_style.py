from stores.base import PizzaStore

from pizzas import HousePizza, VeggiePizza, MeatPizza
from ingredients.chicago_ingredients import ChicagoIngredientFactory


class ChicagoStylePizzaStore(PizzaStore):
   def createPizza(self, pizzaType:str):
      pizza = None
      ingredients = ChicagoIngredientFactory()

      if pizzaType == "house":
         pizza = HousePizza(ingredients)
         pizza.setName("Chicago Style House Pizza")

      if pizzaType == "meat":
         pizza = MeatPizza(ingredients)
         pizza.setName("Chicago Style Meat Pizza")
      
      if pizzaType == "veggie":
         pizza = VeggiePizza(ingredients)
         pizza.setName("Chicago Style Veggie Pizza")

      return pizza