from pizzas.house_pizza import HousePizza
from pizzas.meat_pizza import MeatPizza
from pizzas.pepperoni_pizza import PepperoniPizza
from pizzas.veggie_pizza import VeggiePizza

def __init__():
    return ["HousePizza", 
            "MeatPizza", 
            "PepperoniPizza", 
            "VeggiePizza"]