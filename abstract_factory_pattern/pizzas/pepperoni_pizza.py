from pizzas.base import Pizza
from ingredients.interface import PizzaIngredientFactory

class PepperoniPizza(Pizza):
    def __init__(self, ingredient_factory:PizzaIngredientFactory):
        self.ingredients = ingredient_factory
    
    def prepare(self):
        print("Preparing Pepperoni Pizza")
        self.ingredients.createDough()
        self.ingredients.createSauce()
        self.ingredients.createCheese()
        self.ingredients.createPepperoni()
