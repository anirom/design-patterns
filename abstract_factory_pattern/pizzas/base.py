from abc import abstractmethod, ABCMeta

class Pizza(metaclass=ABCMeta):
    def __init__(self):
        self.name = ""

    @abstractmethod
    def prepare(self):
        pass

    def bake(self):
        print("Baking for 25 minutes at 350")

    def cut(self):
        print("Cutting the pizza into diagonal slice")

    def box(self):
        print("Place pizza in official PizzaStore box")

    def getName(self):
        return self.name

    def setName(self, name:str):
        self.name = name
        