from pizzas.base import Pizza
from ingredients.interface import PizzaIngredientFactory

class HousePizza(Pizza):
    def __init__(self, ingredient_factory:PizzaIngredientFactory):
        self.ingredients = ingredient_factory
    
    def prepare(self):
        print("Preparing House Pizza")
        self.ingredients.createDough()
        self.ingredients.createSauce()
        self.ingredients.createCheese()
        self.ingredients.createVeggies()
        self.ingredients.createMeat()