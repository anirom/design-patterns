from ingredients.interface import PizzaIngredientFactory

from ingredients.dough import NormalDough
from ingredients.sauce import BeansSauce
from ingredients.cheese import OaxacaCheese
from ingredients.veggie import Chilies, Onions, Avocado
from ingredients.meat import Chorizo


class MexicanIngredientFactory(PizzaIngredientFactory):
    def createDough(self):
        return NormalDough()

    def createSauce(self):
        return BeansSauce()

    def createCheese(self):
        return OaxacaCheese()

    def createVeggies(self):
        return [Chilies(), 
                Onions(), 
                Avocado()]

    def createMeat(self):
        return Chorizo()