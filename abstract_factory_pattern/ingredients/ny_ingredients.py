from ingredients.interface import PizzaIngredientFactory

from ingredients.dough import ThinDough
from ingredients.sauce import MarinaraSauce
from ingredients.cheese import MozarellaCheese
from ingredients.veggie import Olives, Onions, Mushrooms, GreenPeppers
from ingredients.meat import Pepperoni


class NYIngredientFactory(PizzaIngredientFactory):
    def createDough(self):
        return ThinDough()

    def createSauce(self):
        return MarinaraSauce()

    def createCheese(self):
        return MozarellaCheese()

    def createVeggies(self):
        return [Olives(), 
                Onions(), 
                Mushrooms(), 
                GreenPeppers()]

    def createPepperoni(self):
        return Pepperoni()

    def createMeat(self):
        return Pepperoni()