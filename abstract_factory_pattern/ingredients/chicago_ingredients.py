from ingredients.interface import PizzaIngredientFactory

from ingredients.dough import ThickDough
from ingredients.sauce import PlumTomatoSauce
from ingredients.cheese import ParmesanCheese
from ingredients.veggie import Onions, Mushrooms, GreenPeppers
from ingredients.meat import GroundBeef, Pepperoni


class ChicagoIngredientFactory(PizzaIngredientFactory):
    def createDough(self):
        return ThickDough()

    def createSauce(self):
        return PlumTomatoSauce()

    def createCheese(self):
        return ParmesanCheese()

    def createVeggies(self):
        return [Onions(), 
                Mushrooms(), 
                GreenPeppers()]

    def createMeat(self):
        return [GroundBeef(), Pepperoni()]