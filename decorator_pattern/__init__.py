from size import Size
from beverages import DarkRoast, Decaf, Espresso, HouseBlend
from condiments import Mocha, Whip, SteamedMilk, Soy

beverage1 = Decaf()
print(f"{beverage1.getDescription()}, {beverage1.cost()}")

beverage2 = DarkRoast()
beverage2 = Whip(beverage2)
print(f"{beverage2.getDescription()}, {'%.2f' % beverage2.cost()}")

beverage3 = Espresso()
beverage3 =  Mocha(beverage3)
beverage3 =  Mocha(beverage3)
beverage3 = Whip(beverage3)
print(f"{beverage3.getDescription()}, {'%.2f' % beverage3.cost()}")

beverage4 = HouseBlend()
beverage4 =  SteamedMilk(beverage4)
print(f"{beverage4.getDescription()}, {'%.2f' % beverage4.cost()}")

beverage5 = Espresso()
beverage5.setSize(Size.VENTI.value)
beverage5 = Soy(beverage5)
print(f"{beverage5.getDescription()}, {'%.2f' % beverage5.cost()}")