from beverage import Beverage

class Condiment(Beverage):
    def __init__(self, beverage:Beverage):
        self.beverage = beverage

    def getDescription(self):
        raise NotImplementedError
       