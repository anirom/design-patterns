from beverages.dark_roast import DarkRoast
from beverages.decaf import Decaf
from beverages.espresso import Espresso
from beverages.house_blend import HouseBlend


def __init__():
    return ["DarkRoast", "Decaf", "Espresso", "HouseBlend"]