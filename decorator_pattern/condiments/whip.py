from condiment import Condiment

class Whip(Condiment):
    def getDescription(self):
        return f"{self.beverage.getDescription()}, Whip"

    def cost(self):
        return self.beverage.cost() + 0.10