from condiment import Condiment

class Mocha(Condiment):
    def getDescription(self):
        return f"{self.beverage.getDescription()}, Mocha"

    def cost(self):
        return self.beverage.cost() + 0.20
