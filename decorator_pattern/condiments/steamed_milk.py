from condiment import Condiment

class SteamedMilk(Condiment):
    def getDescription(self):
        return f"{self.beverage.getDescription()}, Steamed Milk"

    def cost(self):
        return self.beverage.cost() + 0.10