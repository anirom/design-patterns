from size import Size
from condiment import Condiment

class Soy(Condiment):
    def getDescription(self):
        return f"{self.beverage.getDescription()}, Soy"

    def cost(self):
        cost = 0.10 if self.beverage.getSize() == Size.TALL.value else (0.15 if self.beverage.getSize() == Size.GRANDE.value else 0.20)
        return self.beverage.cost() + cost