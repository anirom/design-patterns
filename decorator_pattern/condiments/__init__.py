from condiments.mocha import Mocha
from condiments.soy import Soy
from condiments.steamed_milk import SteamedMilk
from condiments.whip import Whip


def __init__():
    return ["Mocha", "Soy", "SteamedMilk", "Whip"]