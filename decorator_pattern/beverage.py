from size import Size

class Beverage:
    def __init__(self): 
        self.description:str = ""
        self.size = Size.TALL.value

    def cost(self):
        raise NotImplementedError    

    def getDescription(self):
        return f"{self.description} {self.size}"

    def getSize(self):
        return self.size

    def setSize(self, size:str):
        self.size = size
