from enum import Enum

class Size(Enum):
    TALL = "Tall"
    GRANDE = "Grande"
    VENTI = "Venti"
